using UnityEngine;
//using Windows.Kinect;
using System.Collections;
using System;
using System.Collections.Generic;


public class SimpleGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
	// GUI Text to display the gesture messages.
	public GUIText GestureInfo;
	
	// private bool to track if progress message has been displayed
	private bool progressDisplayed;

   
	
	public void UserDetected(long userId, int userIndex)
	{
		// as an example - detect these user specific gestures
		KinectManager manager = KinectManager.Instance;
		manager.DetectGesture(userId, KinectGestures.Gestures.Jump);
		manager.DetectGesture(userId, KinectGestures.Gestures.Squat);
		manager.DetectGesture(userId, KinectGestures.Gestures.Push);
		manager.DetectGesture(userId, KinectGestures.Gestures.Pull);
        manager.DetectGesture(userId, KinectGestures.Gestures.RotateHand);
        manager.DetectGesture(userId, KinectGestures.Gestures.Defence);		
      	manager.DetectGesture(userId, KinectGestures.Gestures.SwipeLeft);
		manager.DetectGesture(userId, KinectGestures.Gestures.SwipeRight);		
		if(GestureInfo != null)
		{
			GestureInfo.guiText.text = "SwipeLeft, SwipeRight, Squat, Push or Pull.";
		}
	}
	
	public void UserLost(long userId, int userIndex)
	{
		if(GestureInfo != null)
		{
			GestureInfo.guiText.text = string.Empty;
		}
	}
	public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                              float progress, KinectInterop.JointType joint, Vector3 screenPos)
	{
		if((gesture == KinectGestures.Gestures.ZoomOut || gesture == KinectGestures.Gestures.ZoomIn) && progress > 0.5f)
		{
			string sGestureText = string.Format ("{0} detected, zoom={1:F1}%", gesture, screenPos.z * 100);

			if(GestureInfo != null)
			{
				GestureInfo.guiText.text = sGestureText;
			}
			//Debug.Log(sGestureText);
			progressDisplayed = true;
		}
		else if(gesture == KinectGestures.Gestures.Wheel && progress > 0.5f)
		{
			string sGestureText = string.Format ("{0} detected, angle={1:F1} deg", gesture, screenPos.z);

			if(GestureInfo != null)
			{
				GestureInfo.guiText.text = sGestureText;
			}

			//Debug.Log(sGestureText);
			 progressDisplayed = true;
		}
	}
      
  
     public   bool forword=false;
    
    void Update()
    {

       
    }  
   

  public GameObject[] handjointsofplayersleft;
  public GameObject[] players; 

     IEnumerator deactivateshield(GameObject shield)
     {
        yield return new WaitForSeconds(5f);
        if (shield != null)
        {
            shield.SetActive(false);
        }
     }
     Dictionary<int, GameObject> swordtouser = new Dictionary<int,GameObject>();

	public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectInterop.JointType joint, Vector3 screenPos)
	{
		string sGestureText = gesture + " detected";
       	if(GestureInfo != null)
        {
            GestureInfo.guiText.text = sGestureText;
            if (gesture == KinectGestures.Gestures.RotateHand)
            {                
                GameObject obj  = handjointsofplayersleft[KinectManager.Instance.alUserIds.IndexOf(userId) ].transform.GetChild(2).gameObject;
                GameObject sword = (GameObject)Instantiate(obj, obj.transform.position, Quaternion.identity);
                sword.transform.parent = handjointsofplayersleft[KinectManager.Instance.alUserIds.IndexOf(userId)].transform;
                sword.transform.localScale = obj.transform.localScale;
                sword.transform.rotation = obj.transform.rotation;
                if (swordtouser.ContainsKey(KinectManager.Instance.alUserIds.IndexOf(userId)))
                {
                    swordtouser.Remove(KinectManager.Instance.alUserIds.IndexOf(userId));
                }
                swordtouser.Add(KinectManager.Instance.alUserIds.IndexOf(userId), sword);

                sword.SetActive(true);     
            }
            if (gesture == KinectGestures.Gestures.Push)
            {
                if (swordtouser.ContainsKey(KinectManager.Instance.alUserIds.IndexOf(userId)))
                {
                    swordtouser[KinectManager.Instance.alUserIds.IndexOf(userId)].transform.parent = null;
                    swordtouser[KinectManager.Instance.alUserIds.IndexOf(userId)].transform.rotation = players[KinectManager.Instance.alUserIds.IndexOf(userId)].transform.rotation;
                    swordtouser[KinectManager.Instance.alUserIds.IndexOf(userId)].AddComponent<Movementscript>();
                    swordtouser[KinectManager.Instance.alUserIds.IndexOf(userId)].AddComponent<swordcollisionScript>();
                }              
               
            }
            if(gesture==KinectGestures.Gestures.Defence)
            {
                GameObject shield = players[KinectManager.Instance.alUserIds.IndexOf(userId)].transform.GetChild(0).gameObject;
                shield.SetActive(true);
                StartCoroutine("deactivateshield",shield);
            }

        }
		
		progressDisplayed = false;
		
		return true;
	}

	public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint)
	{
		if(progressDisplayed)
		{
			// clear the progress info
			if(GestureInfo != null)
			{
				GestureInfo.guiText.text = String.Empty;
			}			
			progressDisplayed = false;
		}
		
		return true;
	}
	
}
