﻿using UnityEngine;
using System.Collections;

public class swordcollisionScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        explosion = Resources.Load("Explosion") as GameObject;

	}
  public  GameObject explosion;
	
	// Update is called once per frame

 
	void Update () 
    {
        
	}
    
    void OnCollisionEnter(Collision colliderobject)
    {
        if (colliderobject.gameObject.tag == "shield")
        {
            Instantiate(explosion, colliderobject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if (colliderobject.gameObject.tag == "Player")
        {
            print("player gets hurt");
            Destroy(gameObject.GetComponent<Movementscript>());
        }

    }
}
